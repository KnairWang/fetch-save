extern crate reqwest;

use crate::config::CmdArgs;
use reqwest::header::HeaderMap;
use reqwest::header::HeaderName;
use reqwest::Response;
use std::fs::File;
use std::io::BufRead;
use std::io::Read;
use std::io::Write;
use std::str::FromStr;

#[tokio::main]
pub async fn start(args: CmdArgs) -> std::result::Result<(), ()> {
    let headers = parse_headers_from_file(&args.header_file);

    let http_result = fetch_data(&args.full_url, headers, args.method, &args.body_file).await;
    match http_result {
        Ok(res) => {
            println!("[INFO] status code: {}", res.status());
            print_headers(res.headers());
            save_to_file(args.output, res).await;
        }
        Err(error) => {
            eprintln!("[WARNING] {}", error);
        }
    }

    Ok(())
}

fn parse_headers_from_file(path: &str) -> HeaderMap {
    let mut headers = HeaderMap::new();

    if let Ok(file) = File::open(path) {
        let reader = std::io::BufReader::new(file);
        for line in reader.lines() {
            if let Some((k, v)) = parse_heaser(line) {
                headers.insert(HeaderName::from_str(&k).unwrap(), v.parse().unwrap());
            }
        }
    } else {
        eprintln!("[WARNING] cannot open headers file: {}", path);
    }

    headers
}

fn parse_heaser(line: std::result::Result<String, std::io::Error>) -> Option<(String, String)> {
    match line {
        Ok(s) => {
            if let Some(i) = s.find(":") {
                let key = &s[..i];
                let value = &s[i + 1..];
                if key.is_empty() {
                    eprintln!("[WARNING] header key is empty: {}", s);
                    None
                } else if value.is_empty() {
                    eprintln!("[WARNING] header value is empty: {}", s);
                    None
                } else {
                    Some((key.to_string(), value.to_string()))
                }
            } else {
                eprintln!("[WARNING] cannot parse line: {}", s);
                None
            }
        }
        Err(error) => {
            eprintln!("[WARNING] cannot read line: {}", error);
            None
        }
    }
}

async fn fetch_data(
    full_url: &str,
    headers: HeaderMap,
    method: reqwest::Method,
    body_file: &str,
) -> std::result::Result<reqwest::Response, String> {
    let mut client = reqwest::ClientBuilder::new()
        .connect_timeout(std::time::Duration::new(3*60, 0))
        .build()
        .unwrap()
        .request(method, full_url)
        .headers(headers);
    if !body_file.is_empty() {
        if let Ok(mut file) = File::open(body_file) {
            let mut file_content = String::new();
            match file.read_to_string(&mut file_content) {
                Ok(s) => {
                    client = client.header("content-length", s);
                }
                Err(error) => {
                    println!("[WARNING] fail to read file content {}", error);
                }
            }
            client = client.body(file_content);
        } else {
            eprintln!("[WARNING] cannot open body file: {}", body_file);
        }
    }

    let res = client.send().await;
    match res {
        Ok(res) => Ok(res),
        Err(_error) => Err(format!("fail to fetch: {}", _error)),
    }
}

fn print_headers(headers: &HeaderMap) {
    println!();
    println!("[INFO] response headers:");
    for (k, v) in headers {
        println!("[INFO] {}: {}", k, v.to_str().unwrap());
    }
    println!();
}

async fn save_to_file(output: String, response: Response) {
    if let Ok(mut file) = std::fs::File::create(output) {
        let bytes = response.bytes().await.unwrap();
        for b in bytes.bytes() {
            let byte = b.unwrap();
            file.write(&[byte]).unwrap();
        }
    } else {
        eprintln!("[WARNING] cannot create file to save body");
    }
}
