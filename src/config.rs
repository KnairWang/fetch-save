use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "fetch-save", about = "save a http response body to file.")]
pub struct CmdArgs {
    #[structopt(short = "u", long, help = "resource full url to fetch")]
    pub full_url: String,

    #[structopt(
        short = "h",
        long,
        help = "a file contains request header, each line contains a pair of header key-value",
        default_value = ""
    )]
    pub header_file: String,

    #[structopt(
        short = "b",
        long,
        help = "a file contains request raw body",
        default_value = ""
    )]
    pub body_file: String,

    #[structopt(short = "m", long, help = "http request method", default_value = "GET")]
    pub method: reqwest::Method,

    #[structopt(
        short = "o",
        long,
        help = "full path to save response body",
        default_value = "response"
    )]
    pub output: String,
}
