mod config;
mod entry;

extern crate reqwest;

use config::CmdArgs;
use structopt::StructOpt;

fn main() {
    let args = CmdArgs::from_args();

    entry::start(args).unwrap();

    println!("[INFO] done");
}
